# Grid position


[![pipeline status](https://gitlab.com/jeremiak/dot-matrix-grid/badges/master/pipeline.svg)](https://gitlab.com/jeremiak/dot-matrix-grid/commits/master)

Sometimes you want to make a grid of objects. Not like a CSS grid, more like a dot matrix data visualization. This library is a helper function that can return the position of a given element in an array.

The function takes two arguments:
* `rowLength` which is the number of items in each row
* `elementIndex` which is the index of the element that you want the position for

It will return an array where the first item represents the column (along the x axis) that item should go in and the second item represents the row (along the y axis) that the item should go in. The first item will be at [1, 1].

```
const getPosition = require('grid-position')

const data = ['a', 'b', 'c', 'd']
const rowLength = 2
const positions = data.map((d, i) => {
  return getPosition(rowLength, i)
})

// positions equals [[1, 1], [1, 2], [2, 1], [2, 2]]
```