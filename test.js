const test = require("tape")

const fn = require("./index")

test("returns proper grid position", t => {
  t.plan(1)

  const data = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]
  const expected = [
    [1, 1],
    [2, 1],
    [1, 2],
    [2, 2],
    [1, 3],
    [2, 3],
    [1, 4],
    [2, 4],
    [1, 5],
    [2, 5]
  ]
  const rowLength = 2
  const positions = data.map((d, i) => fn(rowLength, i))

  t.deepEqual(positions, expected)
})
