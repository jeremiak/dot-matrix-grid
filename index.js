function getCol(rowLength, i) {
  return (i % rowLength) + 1
}

function getRow(rowLength, i) {
  const coef = (i - rowLength) / rowLength + 1
  const ceil = Math.ceil(coef)

  if (coef < 1) {
    return 1
  }
  if (coef === 1) {
    return 2
  }
  if (coef === ceil) {
    return ceil + 1
  }
  
  return ceil
}

function getGridPosition(rowLength, i) {
  return [getCol(rowLength, i), getRow(rowLength, i)]
}

module.exports = getGridPosition
